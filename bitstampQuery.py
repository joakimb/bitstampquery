import json
from StringIO import StringIO
import urllib2
import pusherclient
import sys
# Add a logging handler so we can see the raw communication data
import logging
import time
import requests

root = logging.getLogger()
root.setLevel(logging.CRITICAL)
ch = logging.StreamHandler(sys.stdout)
#root.addHandler(ch)

json_orderbook = json.load(StringIO(urllib2.urlopen("https://www.bitstamp.net/api/order_book/").read()))
json_transactions = json.load(StringIO(urllib2.urlopen("https://www.bitstamp.net/api/transactions/").read()))

global pusher
global url
url = "http://server.carl-johanheinze.se:3000"
#url = "http://localhost:1337"

def connect_handler(data):
	live_trades = pusher.subscribe('live_trades');
	live_trades.bind('trade', transaction_update)
	diff_order_book = pusher.subscribe('diff_order_book')
	diff_order_book.bind('data', orderbook_update)

def transaction_update(data):
	decoded_data = decode(data)
        
        event = {}
	event['type'] = 'transaction'
	event['status'] = 'ok'
	event['exchange_id'] = 'bitstamp'
	event['from_currency'] = 'usd'
	event['to_currency'] = 'btc'
        event['otime'] = time.time()
	event['price'] = decoded_data['price']
	event['volume'] = decoded_data['amount']
        
        events = [event]
        
        dataf = {"exchange_id": "bitstamp", "events" : events}
	send_to_db(encode(dataf))

def orderbook_update(data):
	decoded_data = decode(data)
	events = []
	for bid in decoded_data['bids']:
		new_bid = {}
		new_bid['type'] = 'buy'
		new_bid['status'] = 'ok'
		new_bid['from_currency'] = 'usd'
		new_bid['to_currency'] = 'btc'
		new_bid['otime'] = float(decoded_data["timestamp"])
		new_bid['price'] = bid[0]
		new_bid['volume'] = bid[1]
		events.append(new_bid)
	for ask in decoded_data['asks']:
		new_ask = {}
		new_ask['type'] = 'sell'
		new_ask['status'] = 'ok'
		new_ask['from_currency'] = 'usd'
		new_ask['to_currency'] = 'btc'
		new_ask['otime'] = float(decoded_data["timestamp"])
		new_ask['price'] = ask[0]
		new_ask['volume'] = ask[1]
		events.append(new_ask)
        dataf = {"exchange_id": "bitstamp", "events" : events}
	send_to_db(encode(dataf))

pusher = pusherclient.Pusher('de504dc5763aeef9ff52')
pusher.connection.bind('pusher:connection_established', connect_handler)
pusher.connect()

def send_to_db(data):
        print "sending/..."
	print requests.post(url, data)
        print "done"

def decode(data):
	return json.load(StringIO(data))

def encode(data):
	return json.dumps(data);

while True:
	time.sleep(1)
